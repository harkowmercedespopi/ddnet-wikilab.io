---
title: "Front Layer"
explain: true
weight: 30
---

#### Tile representations of the 'front' layer in the editor.

Secondary tile layer of maps. Excludes collision blocks and adds tiles that are only relevant when layed over other tiles.

{{% explain file="static/explain/front.svg" %}}
