#!/bin/bash
# requires ffmpeg

set -o errexit -o pipefail -o noclobber -o nounset

if [ "$#" -lt 6 ]; then
	echo "usage: $0 INPUT_FILE OUTPUT_FILE TILES_UP TILES_DOWN TILES_LEFT TILES_RIGHT [FFMPEG_ARGS]"
	echo "examples:"
	echo "  - crop video with default settings"
	echo "    $0 tutorial.demo.mp4 tutorial.mp4 5 5 7 7"
	echo "  - crop video starting from second one with a duration of 3 seconds and disabling audio"
	echo "    $0 tutorial.demo.mp4 tutorial.mp4 5 5 7 7 -ss 1 -t 3 -an"
	exit -1
fi

# Output parameters 
INP_FILE="$1"
OUT_FILE="$2"
FFMPEG_ARGS="${@:3}"

# This has to be determined per input video (input parameters)
# 1920:1080 -> 43
# 1680:1050 -> 39
PIXEL_PER_TILE=43

TILES_UP=$3
TILES_DOWN=$4
TILES_LEFT=$5
TILES_RIGHT=$6

# Get video dimensions
DIMENSIONS=$(ffprobe -v error -show_entries stream=width,height -of default=noprint_wrappers=1 ${INP_FILE})
eval ${DIMENSIONS^^} # upper case variable names: WIDTH, and HEIGHT

echo $WIDTH $HEIGHT

# cut videos
OUT_WIDTH=$(( ($TILES_LEFT + $TILES_RIGHT + 1) * $PIXEL_PER_TILE ))
OUT_HEIGHT=$(( ($TILES_UP + $TILES_DOWN + 1) * $PIXEL_PER_TILE ))
# calculate top left pixel
OUT_X=$(( ($WIDTH - $PIXEL_PER_TILE) / 2 - $TILES_LEFT * $PIXEL_PER_TILE))
OUT_Y=$(( ($HEIGHT - $PIXEL_PER_TILE) / 2 - $TILES_UP * $PIXEL_PER_TILE))

ffmpeg $FFMPEG_ARGS \
	-i "$INP_FILE" \
	-vf "crop=${OUT_WIDTH}:${OUT_HEIGHT}:${OUT_X}:${OUT_Y}" \
	"$OUT_FILE"

